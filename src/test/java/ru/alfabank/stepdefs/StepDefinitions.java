package ru.alfabank.stepdefs;

import cucumber.api.Scenario;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import ru.alfabank.pages.*;

public class StepDefinitions {
    private WebDriver driver;
    private int numberElementsOnPage;
    private String firstElementInList;
    private String productName;

    @cucumber.api.java.Before
    public void beforeScenario(Scenario scenario){
        System.setProperty("webdriver.chrome.driver", ".\\src\\test\\resources\\chromedriver.exe");
        driver = new ChromeDriver();
    }

    @cucumber.api.java.After
    public void afterScenario(Scenario scenario){
        driver.quit();
    }

    @Given("^Open browser and maximize$")
    public void openBrowserAndMaximize(){
       Browser.openBrowser(driver);
    }

    @When("^Go to yandex$")
    public void goToYandex() {
        YandexPage.openYandexPage(driver);
    }

    @And("^Go to Yandex Market$")
    public void goToYandexMarket() {
        YandexPage.clickMarketLink(driver);
    }

    @And("^Choose section \"([^\"]*)\"$")
    public void chooseSectionElectronics(String catalog) {
        YandexMarketPage.clickCatalog(driver, catalog);
    }

    @And("^Select a section \"([^\"]*)\"$")
    public void selectASection(String section) {
        YandexMarketElectronics.clickSection(driver, section);
    }

    @And("^Go to advanced search$")
    public void goToAdvancedSearch() {
        Buttons.clickButtonAllFilters(driver);
    }

    @And("^Set the search parameter from \"([^\"]*)\" rubles$")
    public void setSearchParameterFromRubles(String price) {
        Filters.setPriceFrom(driver, price);
    }

    @And("^Select manufacturers \"([^\"]*)\" and \"([^\"]*)\"$")
    public void selectManufacturersAppleAndSamsung(String brand1, String brand2) {
        Filters.setBrand(driver, brand1);
        Filters.setBrand(driver, brand2);
    }

    @And("^Select manufacturers \"([^\"]*)\"$")
    public void selectManufacturersBeats(String brand) {
        Filters.setBrand(driver, brand);
    }

    @And("^Click Apply button$")
    public void clickApplyButton() {
        Buttons.clickButtonShowSuitable(driver);
    }

    @Then("^Check that the elements on page 12$")
    public void checkThatTheElementsOnPage12() {
        numberElementsOnPage = Elements.getListElements(driver);
        int expectedResult = 12;
        Assert.assertEquals(expectedResult, numberElementsOnPage);
    }

    @And("^Remember first element in list$")
    public void rememberFirstItemInList() {
        firstElementInList = Elements.getNthElementList(driver, 0);
    }

    @And("^Enter the stored value in search field$")
    public void enterTheStoredValueInTheSearchField() {
        Filters.setValueInSearchField(driver, firstElementInList);
    }

    @Then("^Find and verify that the product name matches the stored value$")
    public void findAndVerifyThatTheProductNameMatchesTheStoredValue() {
        Buttons.clickSearchButton(driver);
        productName = Elements.getNameProduct(driver);
        Assert.assertEquals(productName, firstElementInList);
    }

    @And("^Select sorting \"([^\"]*)\"$")
    public void selectSortingByPrice(String byPrice) {
        Filters.selectSortingBy(driver, byPrice);
    }

    @Then("^Check that the elements on the page are sorted correctly$")
    public void checkThatTheElementsOnPageAreSortedCorrectly() {
        Elements.WaitForPageLoaded(driver);
        boolean expectedResult = true;
        boolean sortElement = Elements.getSortByPriceList(driver);
        Assert.assertEquals(expectedResult, sortElement);
    }
}
