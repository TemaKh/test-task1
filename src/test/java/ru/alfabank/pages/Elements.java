package ru.alfabank.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

public class Elements {
    private static JavascriptExecutor jse;
    private static List<WebElement> listElements;
    private static List<WebElement> sortListByPrice;
    private static By element = By.xpath("//div[@class='n-snippet-cell2__header']/div[@class='n-snippet-cell2__title']");
    private static By nameTitle = By.xpath("//h1");
    private static By price = By.xpath("//div[@class='n-snippet-cell2__main-price-wrapper']//div[@class='price']");

    public static int getListElements(WebDriver driver) {
        listElements = driver.findElements(element);
        return listElements.size();
    }

    public static String getNthElementList(WebDriver driver, int index) {
        listElements = driver.findElements(element);
        return listElements.get(index).getText();
    }

    public static String getNameProduct(WebDriver driver) {
        return driver.findElement(nameTitle).getText();
    }

    public static boolean getSortByPriceList(WebDriver driver) {
        sortListByPrice = driver.findElements(price);
        List<Integer> sortList = new ArrayList<>();
        for (WebElement element : sortListByPrice) {
            sortList.add(Integer.parseInt(element.getText().replaceAll("[^0-9]", "")));
        }
        for (int i = 0; i < sortList.size() - 1; i++) {
            if (sortList.get(i) > sortList.get(i + 1)) {
                return false;
            }
        }
        return true;
    }

    public static void WaitForPageLoaded(WebDriver driver)
    {
        jse = (JavascriptExecutor)driver;
        jse.executeAsyncScript("window.setTimeout(arguments[arguments.length - 1], 5000);");
    }
}
