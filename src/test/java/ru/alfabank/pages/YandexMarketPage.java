package ru.alfabank.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class YandexMarketPage {
    private static String allCatalogs = "//noindex/div[2]//a/span[text()='%s']";

    public static void clickCatalog(WebDriver driver, String catalog) {
        driver.findElement(By.xpath(String.format(allCatalogs, catalog))).click();
    }
}
