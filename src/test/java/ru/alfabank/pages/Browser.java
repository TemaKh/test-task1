package ru.alfabank.pages;

import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

public class Browser {
    public static void openBrowser(WebDriver driver) {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }
}
