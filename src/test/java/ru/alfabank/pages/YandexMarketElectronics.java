package ru.alfabank.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class YandexMarketElectronics {
    private static String section = "//*[@class='_1Y6X2G3jjK']//a[text()='%s']";

    public static void clickSection(WebDriver driver, String sectionName) {
        driver.findElement(By.xpath(String.format(section, sectionName))).click();
    }
}
