package ru.alfabank.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class YandexPage {
    private static String yandex = "https://yandex.ru/";
    private static By marketLink = By.cssSelector("[data-id='market']");

    public static void openYandexPage(WebDriver driver) {
        driver.get(yandex);
    }

    public static void clickMarketLink(WebDriver driver) {
        driver.findElement(marketLink).click();
    }
}
