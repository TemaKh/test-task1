package ru.alfabank.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Filters {
    private static By fieldPriceFrom = By.xpath("//*[@id='glf-pricefrom-var']");
    private static String checkbox = "//label[text()='%s']";
    private static By searchField = By.xpath(".//*[@id='header-search']");
    private static String sortingBy = ".//a[text()='%s']";

    public static void setPriceFrom(WebDriver driver, String price) {
        driver.findElement(fieldPriceFrom).sendKeys(price);
    }

    public static void setBrand(WebDriver driver, String brand) {
        driver.findElement(By.xpath(String.format(checkbox, brand))).click();
    }

    public static void setValueInSearchField(WebDriver driver, String value) {
        driver.findElement(searchField).sendKeys(value);
    }

    public static void selectSortingBy(WebDriver driver, String sortBy) {
        driver.findElement(By.xpath(String.format(sortingBy, sortBy))).click();
    }
}
