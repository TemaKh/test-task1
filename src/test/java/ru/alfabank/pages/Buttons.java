package ru.alfabank.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Buttons {
    private static JavascriptExecutor jse;
    private static By buttonAllFilters = By.xpath("//*[@class='OcaftndW9c _2bjY2zQo59 _4WmLhr2Vhx _2Kihe5N2Sn']");
    private static By buttonShowSuitable = By.xpath("//*[@class='button button_size_l button_theme_pseudo " +
            "i-bem button_action_show-filtered n-filter-panel-extend__controll-button_size_big button_js_inited']");
    private static By searchButton = By.xpath("//*[@type='submit']");

    public static void clickButtonAllFilters(WebDriver driver) {
        jse = (JavascriptExecutor)driver;
        WebElement allFilters = driver.findElement(buttonAllFilters);
        jse.executeScript("arguments[0].scrollIntoView(true);", allFilters);
        allFilters.click();
    }

    public static void clickButtonShowSuitable(WebDriver driver) {
        jse = (JavascriptExecutor)driver;
        WebElement showSuitable = driver.findElement(buttonShowSuitable);
        jse.executeScript("arguments[0].scrollIntoView(true);", showSuitable);
        showSuitable.click();
    }

    public static void clickSearchButton(WebDriver driver) {
        driver.findElement(searchButton).click();
    }
}
