Feature: Using Yandex Market
  @All
  @Phones
  Scenario: Check that the name of the phone corresponds to the memorized value
    Given Open browser and maximize
    When Go to yandex
    And Go to Yandex Market
    And Choose section "Электроника"
    And Select a section "Мобильные телефоны"
    And Go to advanced search
    And Set the search parameter from "20000" rubles
    And Select manufacturers "Apple" and "Samsung"
    And Click Apply button
    Then Check that the elements on page 12
    And Remember first element in list
    And Enter the stored value in search field
    Then Find and verify that the product name matches the stored value

  @All
  @Headphones
  Scenario: Check that the name of the headphones corresponds to the memorized value
    Given Open browser and maximize
    When Go to yandex
    And Go to Yandex Market
    And Choose section "Электроника"
    And Select a section "Наушники и Bluetooth-гарнитуры"
    And Go to advanced search
    And Set the search parameter from "5000" rubles
    And Select manufacturers "Beats"
    And Click Apply button
    Then Check that the elements on page 12
    And Remember first element in list
    And Enter the stored value in search field
    Then Find and verify that the product name matches the stored value

  @All
  @SortedPhonesByPrice
  Scenario: Check that the elements on the page are sorted correctly
    Given Open browser and maximize
    When Go to yandex
    And Go to Yandex Market
    And Choose section "Электроника"
    And Select a section "Мобильные телефоны"
    And Select sorting "по цене"
    Then Check that the elements on the page are sorted correctly